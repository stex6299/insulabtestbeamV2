import serial
from serial import *
from datetime import datetime
from time import sleep



# Definisco la porta seriale e le impostazioni
ser = serial.Serial("/dev/ttyUSB0", baudrate = 115200, bytesize = EIGHTBITS, stopbits = STOPBITS_ONE, parity = PARITY_NONE, timeout = 5)

# Apro la porta se non gia' aperta
if not ser.isOpen():
        ser.open()

# Entriamo in modalita' macchina
ser.write("AT+MACHINE\r\n".encode())



print("\n*** Welcome to tensionLogger.py :) ***")



outFile = "Tension.log"


"""
ser.write("AT+GET,231\r\n".encode())
tmp = ser.read(100).decode()
print("ADC readback voltage: " + str(tmp).replace("OK", "").replace("=", "").strip() + " V")


ser.write("AT+GET,232\r\n".encode())
tmp = ser.read(100).decode()
print("ADC readback current: " + str(tmp).replace("OK", "").replace("=", "").strip() + " mA")
"""






while(True):

        # Tensione
        ser.write("AT+GET,231\r\n".encode())
        tmp = ser.read(100).decode()
        myVolt = str(tmp).replace("OK", "").replace("=", "").strip()

        # Corrente
        ser.write("AT+GET,232\r\n".encode())
        tmp = ser.read(100).decode()
        myCurr = str(tmp).replace("OK", "").replace("=", "").strip()



        try:
                print(str(datetime.now()) + "\t" + myVolt + "\t" + myCurr + "\n")

                with open(outFile, "a") as f:
                        f.write(str(datetime.now()))
                        f.write("\t")
                        f.write(myVolt)
                        f.write("\t")
                        f.write(myCurr)
                        f.write("\n")


        except Exception as e:
                print(e)


        sleep(60)