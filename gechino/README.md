# QUESTA DOCUMENTAZIONE VA CREATA E INTEGRATA
# What's here
Script to control the HV power supply *caen DT5485P*, used for biasing SiPM matrices, friendly called GECHINO. It has to be connected via USB to the DAQ PC, it's in this repository just because it is used in this context

- [accendi.py](accendi.py): Set the Tension (in the range **20-85V**) and enable the output. At the moment you have to set in the code, after the variable `voltage`.
- [spegni.py](spegni.py): Disable the output.
- [status.py](status.py): Display the current voltage and current.
- [logger.py](logger.py): Log the timestamp, the current tension and current values and save them in a file `Tension.log`. One can set the time between two different logs. At this moment is 1 min. In the future I plan to create different files, or something like that. NOTE: Should be called in a tmux session or something like that, in order to let him run.
- [ACCENDI.sh](ACCENDI.sh): Call `accendi.py` and `status.py`.
- [SPEGNI.sh](ACCENDI.sh): Call `spegni.py` and `status.py`.


