import h5py
import numpy as np
import sys


with h5py.File(f"/eos/project/i/insulab-como/testBeam/TB_H2_2022_09/HDF5/run{sys.argv[1]}.h5", 'r', libver='latest', swmr=True) as hf:
    nEv = hf[f"Ievent"]
    print (nEv.shape)