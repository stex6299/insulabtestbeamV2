# What's here
You can find here some useful python functions

- `loadSpillAscii.py` allows you to load all the spills referred to a given run. It is meant to be implemented in a real script.
- `hdfToRoot.py` allow you to produce ROOT TTree files from HDF5 files. It is directly callable. Take a look at lines 38-41 to decide if input and output files should be passed as command line arguments or hard coded in the script.
- [`lineCount.py`](./lineCount.py) allows you to count the acquired data in a file (with waveform). Call as `py lineCount.py 530178`. Alternative way: `R=$(wc -l ASCII_SSHFS/run530178* | awk '{ print $1 }' | tail -1); echo $((R/9))`
- [`convertAllHDF5ToRoot.py`](./convertAllHDF5ToRoot.py) Crea tanti root files da 10k eventi ciascuno. Non è robusto, è una soluzione temporanea di tamponamento per klever21. si deve pregare che non si blocchi. alla fine andrà chiamato un hadd. Questo script è girato in circa 2 ore. Al termine, chiamare [`mergeRoots.py`](./mergeRoots.py), girato in una ventina di minuti o poco più