#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 11 17:06:38 2023

@author: steca
"""


# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


#%% Import moduli
import numpy as np
import time, sys, glob, re, os
import h5py
import uproot


#%% Settings - Percorsi dei files
"""
* inpath: cartella di origine, con gli HDF5
* inpath: cartella di destinazione, con i root files

"""


inpath = r"/eos/project/i/insulab-como/testBeam/TB_H2_2021_08/HDF5"
outpath = r"/eos/project/i/insulab-como/testBeam/TB_H2_2021_08/HDF5_aligned"
# inpath = r"./in"
# outpath = r"./out"
# inpath = sys.argv[0]
# outpath = sys.argv[1]


#%% Funzione

def allineah5(infile, outfile):
    chunkSize = 10000 # Numero di linee per volta da leggere
    
    # Capisco quante righe ha il file
    with h5py.File(infile, 'r', libver='latest',swmr=True) as hf:
            Ievent = np.array(hf["Ievent"])
    numEvents = Ievent.shape[0]
    
    # Numero di volte che leggo un blocco da 10.000
    numIterazioni = int(np.ceil(numEvents / chunkSize))
    
    
    offset = 127
    startind = 24

    esci = False
    
    for i in range(numIterazioni):
        
        inizio = i * chunkSize
        fine = (i+1) * chunkSize
        
        inizio_1742 = inizio + offset
        fine_1742 = fine + offset

        # Se ho meno di 10000 ev --> Carico tutto in un colpo solo
        if numEvents < chunkSize:
            # Carico fino alla fine
            fine_1742 = numEvents 
            fine = numEvents - offset
            
        # Se è l'ultimo ciclo da effettuare (il 2 non è necessario, è per avere un po' di margine in più)
        elif (numEvents - fine_1742) < 2*offset:
            # Vado fino alla fine
            fine_1742 = numEvents
            fine = numEvents - offset
            
            esci = True
        
        
        # Ottengo un chunk di files, allineati
        with h5py.File(infile, 'r', libver='latest',swmr=True) as datafile:
            xpos        = np.array(datafile["xpos"][inizio:fine,:], dtype = np.float32)
            nstrip      = np.array(datafile["nstrip"][inizio:fine,:], dtype = np.uint8)
            nclu        = np.array(datafile["nclu"][inizio:fine,:], dtype = np.uint8)
            
            digi_ph     = np.array(datafile["digi_ph"][inizio:fine, :startind], dtype = np.uint16)
            digi_time   = np.array(datafile["digi_time"][inizio:fine, :startind], dtype = np.uint16)
            digi_base   = np.array(datafile["digi_base"][inizio:fine, :startind], dtype = np.uint16)

            digi_base_1742   = np.array(datafile["digi_base"][inizio_1742:fine_1742, startind:], dtype = np.uint16)
            digi_ph_1742     = np.array(datafile["digi_ph"][inizio_1742:fine_1742, startind:], dtype = np.uint16)
            digi_time_1742   = np.array(datafile["digi_time"][inizio_1742:fine_1742, startind:], dtype = np.uint16)
            
            xinfo       = np.array(datafile["xinfo"][inizio:fine,:], dtype = np.float32)
            info_plus   = np.array(datafile["info_plus"][inizio:fine,:], dtype = np.uint64)
            Ievent      = np.array(datafile["Ievent"][inizio:fine], np.uint64)
            charge      = np.array(datafile["charge"][inizio:fine,:], dtype = np.float32)
            
            wf0         = np.array(datafile["wf0"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf1         = np.array(datafile["wf1"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf2         = np.array(datafile["wf2"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf3         = np.array(datafile["wf3"][inizio_1742:fine_1742, :], dtype = np.uint16)
            
            wf4         = np.array(datafile["wf4"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf5         = np.array(datafile["wf5"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf6         = np.array(datafile["wf6"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf7         = np.array(datafile["wf7"][inizio_1742:fine_1742, :], dtype = np.uint16)
    
    
    
        # Riunisco le ph
        digi_ph   = np.hstack((digi_ph, digi_ph_1742))
        digi_time   = np.hstack((digi_time, digi_time_1742))
        digi_base   = np.hstack((digi_base, digi_base_1742))
        
        
        # Applico il taglio in numero di cluster e per sicurezza chiedo che il numero di evento
        # e di spill siano valori positivi o nulli (non dovrebbero mai essere negativi...)
        nsili_cut = 6
        condmap = np.all((nclu[:,:nsili_cut] == 1), axis = 1)
        cond = (condmap == 1) & (info_plus[:,0] >= 0) & (Ievent >= 0)
        
        
        # ====================================================================
        # Dumpo i dati nel nuovo file
        # ====================================================================     
        
        righeLastFile = xpos.shape[0]
        
        with h5py.File(outfile, 'a', libver='latest') as hf:
            print(outfile)
        
            hf.swmr_mode = True
            
            
            # Se il file non esisteva
            opts = {"compression":"gzip", "chunks":True}
            
            if (len(hf.keys())) == 0:
                
                
                # H2 Settings
                hf.create_dataset("xpos", data =  xpos, maxshape=(None,8), **opts)
                hf.create_dataset("nstrip", data =  nstrip, maxshape=(None,8), **opts)
                hf.create_dataset("nclu", data =  nclu, maxshape=(None,8), **opts)
                
                hf.create_dataset("digi_base", data =  digi_base, maxshape=(None,32), **opts)
                hf.create_dataset("digi_ph", data =  digi_ph, maxshape=(None,32), **opts)
                hf.create_dataset("digi_time", data =  digi_time, maxshape=(None,32), **opts)
                
                hf.create_dataset("xinfo", data =  xinfo, maxshape=(None,5), **opts)
                hf.create_dataset("info_plus", data =  info_plus, maxshape=(None,2), **opts)
                hf.create_dataset("Ievent", data =  Ievent,  maxshape=(None,), **opts)
                hf.create_dataset("charge", data =  charge,  maxshape=(None,8), **opts)

                hf.create_dataset("wf0", data =  wf0,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf1", data =  wf1,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf2", data =  wf2,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf3", data =  wf3,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf4", data =  wf4,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf5", data =  wf5,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf6", data =  wf6,  maxshape=(None,1031), **opts)
                hf.create_dataset("wf7", data =  wf7,  maxshape=(None,1031), **opts)
                
                
                
            else:
                # ================================================================
                # Provo a capire quant'è la minima shape
                # A volte crasha a metà e, nel crashare, aggiorna solamente alcune colonne
                # Andrà inserito nello script principale....

                tmpMin = np.nan

                for k in hf.keys():
                    if np.isnan(tmpMin):
                        tmpMin = hf[k].shape[0]
                    else:
                        if hf[k].shape[0] < tmpMin:
                            tmpMin = hf[k].shape[0]

                for k in hf.keys():
                    #hf[k].resize((hf[k].shape[0] + righeLastFile), axis = 0)
                    hf[k].resize((tmpMin + righeLastFile), axis = 0)

                # In questo modo dovrebbe essere robusto
                # =======================================================
                    
                
                # H2 Settings
                hf["xpos"][-righeLastFile:] = xpos
                hf["nstrip"][-righeLastFile:] = nstrip
                hf["nclu"][-righeLastFile:] = nclu
                
                hf["digi_base"][-righeLastFile:] = digi_base
                hf["digi_ph"][-righeLastFile:] = digi_ph
                hf["digi_time"][-righeLastFile:] = digi_time
                
                hf["xinfo"][-righeLastFile:] = xinfo
                hf["info_plus"][-righeLastFile:] = info_plus
                hf["Ievent"][-righeLastFile:] = Ievent
                hf["charge"][-righeLastFile:] = charge

                hf["wf0"][-righeLastFile:] = wf0
                hf["wf1"][-righeLastFile:] = wf1
                hf["wf2"][-righeLastFile:] = wf2
                hf["wf3"][-righeLastFile:] = wf3
                hf["wf4"][-righeLastFile:] = wf4
                hf["wf5"][-righeLastFile:] = wf5
                hf["wf6"][-righeLastFile:] = wf6
                hf["wf7"][-righeLastFile:] = wf7
                
            
        #print(f"Ho scritto {outfile}")


        
    
    
        # Flag per skippare l'ultima iterazione
        if esci: break
    
    
    
#%%


            
#%% Big loop

# Liste di tutti i files
lstofH5 = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(inpath,"*.h5"))]
#lstofTree = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(outpath,"*.ROOT"))]



# Ciclo su tutti i files
for i in lstofH5:
    
    # Controllo che non esista già il Tree
    #if not i in lstofTree:
    infile = os.path.join(inpath, f"run{i}.h5")
    outfile = os.path.join(outpath, f"run{i}.h5")
    
    print(f"\n--> Numero run: {i}")
    
    

    while(True):
        try:
            print(f"Sto per scrivere {outfile}")
            allineah5(infile, outfile)
            break

        except Exception as e: 
            print(e)
            print("Mannaggia, ci riproviamo tra un secondo")
            time.sleep(1)
            #continue
                
print("Ho finito :)")


