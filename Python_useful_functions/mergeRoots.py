#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan 10 04:51:07 2023

@author: steca
"""

# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


#%% Import moduli
import numpy as np
import time, sys, glob, re, os
import h5py
import uproot

#%% Settings - Percorsi dei files
inpath = r"/eos/project/i/insulab-como/testBeam/TB_H2_2021_08/TREE_Repr_Shifted_Oneclu"
outpath = r"/eos/project/i/insulab-como/testBeam/TB_H2_2021_08/TREE_Repr_Shifted_Oneclu_merged"


lstRun = np.array([int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(inpath,"*_0000.ROOT"))])


for i in lstRun:
    
    lstRoots = glob.glob(os.path.join(inpath,f"run{i}_*.ROOT"))
    
    if len(lstRoots) > 0:
        #print(f"Vorrei scrivere {i}")
        
        comando = "hadd " + os.path.join(outpath, f"run{i}.ROOT") + " " + " ".join(lstRoots)
        
        #print(f"Lancerei il comando\n" + comando + "\n")
        os.system(comando)