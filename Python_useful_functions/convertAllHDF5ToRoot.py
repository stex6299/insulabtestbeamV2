#!/usr/bin/env python3
"""
Questo script permette di convertire un'intera cartella di HDF5 in ROOT files
Non è uno script fatto bene, è stato scritto di corsa per processare per Matt
i dati Klever 21 riprocessati

Concettualmente
- Ciclo su ciascun H5 esistente
- Effettuo lo shifting e il taglio one clu. Questa parte è stata copiata da uno
  script fornito dal buon PMG: se ci fossero problemi, colpa sua
- Dumpo in un root file

Questo script è basato mettendo assieme hdfToRoot e un esempio di pietro
per allineare i dati
"""

# Allows debug on spyder in my spyder-cf environment
import collections
collections.Callable = collections.abc.Callable


#%% Import moduli
import numpy as np
import time, sys, glob, re, os
import h5py
import uproot


#%% Settings - Percorsi dei files
"""
* inpath: cartella di origine, con gli HDF5
* inpath: cartella di destinazione, con i root files

"""


inpath = r"/eos/project/i/insulab-como/testBeam/TB_H2_2021_08/HDF5"
outpath = r"/eos/project/i/insulab-como/testBeam/TB_H2_2021_08/TREE_Repr_Shifted_Oneclu"
# inpath = r"./in"
# outpath = r"./out"
# inpath = sys.argv[0]
# outpath = sys.argv[1]


#%% Funzione

def hdf2Tree_10k(infile, outfile):
    chunkSize = 10000 # Numero di linee per volta da leggere
    
    # Capisco quante righe ha il file
    with h5py.File(infile, 'r', libver='latest',swmr=True) as hf:
            Ievent = np.array(hf["Ievent"])
    numEvents = Ievent.shape[0]
    
    # Numero di volte che leggo un blocco da 10.000
    numIterazioni = int(np.ceil(numEvents / chunkSize))
    
    
    offset = 127
    startind = 24

    esci = False
    
    for i in range(numIterazioni):
        
        inizio = i * chunkSize
        fine = (i+1) * chunkSize
        
        inizio_1742 = inizio + offset
        fine_1742 = fine + offset

        # Se ho meno di 10000 ev --> Carico tutto in un colpo solo
        if numEvents < chunkSize:
            # Carico fino alla fine
            fine_1742 = numEvents 
            fine = numEvents - offset
            
        # Se è l'ultimo ciclo da effettuare (il 2 non è necessario, è per avere un po' di margine in più)
        elif (numEvents - fine_1742) < 2*offset:
            # Vado fino alla fine
            fine_1742 = numEvents
            fine = numEvents - offset
            
            esci = True
        
        
        # Ottengo un chunk di files, allineati
        with h5py.File(infile, 'r', libver='latest',swmr=True) as datafile:
            xpos        = np.array(datafile["xpos"][inizio:fine,:], dtype = np.float32)
            nstrip      = np.array(datafile["nstrip"][inizio:fine,:], dtype = np.uint8)
            nclu        = np.array(datafile["nclu"][inizio:fine,:], dtype = np.uint8)
            
            digi_ph     = np.array(datafile["digi_ph"][inizio:fine, :startind], dtype = np.uint16)
            digi_time   = np.array(datafile["digi_time"][inizio:fine, :startind], dtype = np.uint16)
            digi_base   = np.array(datafile["digi_base"][inizio:fine, :startind], dtype = np.uint16)

            digi_base_1742   = np.array(datafile["digi_base"][inizio_1742:fine_1742, startind:], dtype = np.uint16)
            digi_ph_1742     = np.array(datafile["digi_ph"][inizio_1742:fine_1742, startind:], dtype = np.uint16)
            digi_time_1742   = np.array(datafile["digi_time"][inizio_1742:fine_1742, startind:], dtype = np.uint16)
            
            xinfo       = np.array(datafile["xinfo"][inizio:fine,:], dtype = np.float32)
            info_plus   = np.array(datafile["info_plus"][inizio:fine,:], dtype = np.uint64)
            Ievent      = np.array(datafile["Ievent"][inizio:fine], np.uint64)
            charge      = np.array(datafile["charge"][inizio:fine,:], dtype = np.float32)
            
            wf0         = np.array(datafile["wf0"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf1         = np.array(datafile["wf1"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf2         = np.array(datafile["wf2"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf3         = np.array(datafile["wf3"][inizio_1742:fine_1742, :], dtype = np.uint16)
            
            wf4         = np.array(datafile["wf4"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf5         = np.array(datafile["wf5"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf6         = np.array(datafile["wf6"][inizio_1742:fine_1742, :], dtype = np.uint16)
            wf7         = np.array(datafile["wf7"][inizio_1742:fine_1742, :], dtype = np.uint16)
    
    
    
        # Riunisco le ph
        digi_ph   = np.hstack((digi_ph, digi_ph_1742))
        digi_time   = np.hstack((digi_time, digi_time_1742))
        digi_base   = np.hstack((digi_base, digi_base_1742))
        
        
        # Applico il taglio in numero di cluster e per sicurezza chiedo che il numero di evento
        # e di spill siano valori positivi o nulli (non dovrebbero mai essere negativi...)
        nsili_cut = 6
        condmap = np.all((nclu[:,:nsili_cut] == 1), axis = 1)
        cond = (condmap == 1) & (info_plus[:,0] >= 0) & (Ievent >= 0)
        
        
        # ====================================================================
        # Dumpo i dati nel tree
        # ====================================================================        
        
        output_file = outfile.replace(".ROOT", f"_{i:04d}.ROOT")
        print(f"Sto per scrivere {output_file}")
        with uproot.recreate(output_file) as f:
            f["t"] = {
                "xpos": xpos[cond],
                "nstrip": nstrip[cond],
                "nclu": nclu[cond],
                
                "digi_ph": digi_ph[cond],
                "digi_time": digi_time[cond],
                "digi_base": digi_base[cond],
                
                "xinfo": xinfo[cond],
                "info_plus": info_plus[cond],
                "Ievent": Ievent[cond],
                "charge": charge[cond],
                
                "wf0": wf0[cond],               
                "wf1": wf1[cond],               
                "wf2": wf2[cond],               
                "wf3": wf3[cond],               
                
                "wf4": wf4[cond],               
                "wf5": wf5[cond],               
                "wf6": wf6[cond],               
                "wf7": wf7[cond],               
                
            }
        print(f"Ho scritto {output_file}")


        
    
    
        # Flag per skippare l'ultima iterazione
        if esci: break
    
    
    
#%%


            
#%% Big loop

# Liste di tutti i files
lstofH5 = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(inpath,"*.h5"))]
#lstofTree = [int(re.split(r"\\|/", f)[-1][3:9]) for f in glob.glob(os.path.join(outpath,"*.ROOT"))]



# Ciclo su tutti i files
for i in lstofH5:
    
    # Controllo che non esista già il Tree
    #if not i in lstofTree:
    infile = os.path.join(inpath, f"run{i}.h5")
    outfile = os.path.join(outpath, f"run{i}.ROOT")
    
    print(f"\n--> Numero run: {i}")
    
    

    while(True):
        try:
            print(f"Sto per scrivere {outfile}")
            hdf2Tree_10k(infile, outfile)
            break

        except Exception as e: 
            print(e)
            print("Mannaggia, ci riproviamo tra un secondo")
            time.sleep(1)
            #continue
                
print("Ho finito :)")








