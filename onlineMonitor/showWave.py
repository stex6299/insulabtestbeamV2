# -*- coding: utf-8 -*-
"""
Created on Fri Sep 30 09:32:45 2022

@author: steca
"""

#%% LIBRARIES
import numpy as np
import os
import pandas as pd

import json
import re
import time
import sys
import glob

import h5py
from io import StringIO

from matplotlib import pyplot as plt
import copy

#%%
lstWf = [1,2,3]
numWf = len(lstWf)

vectWf = []

with h5py.File("./run530082.h5", 'r') as hf:
    for i in range(numWf):
        vectWf.append(np.array(hf[f"wf{i}"]))
        
    nEv = np.array(hf[f"Ievent"])


#%% Subplots
lineVect = []

fig, ax = plt.subplots(numWf, 1)
fig.set_size_inches(12,5)

j=0
for i in range(numWf):
    myLine, = ax[i].plot(vectWf[i][j,:], c = "tab:green", label = f"wf{i}")
    lineVect.append(copy.copy(myLine))
    
fig.canvas.draw()
fig.canvas.flush_events()   



for j in range(nEv.shape[0]):
    #plt.close("all")


    
    fig.suptitle(f"Ev: {j}")


    for i in range(numWf):
        lineVect[i].set_ydata(vectWf[i][j,:])
        
    for a in ax:
        a.grid()
        a.legend()

        
    
    fig.canvas.draw()
    fig.canvas.flush_events()   
    input("Press Enter to continue...")
    

    
    
    
    