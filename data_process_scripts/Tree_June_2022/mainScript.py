# -*- coding: utf-8 -*-
"""
Created on Sun Jun 19 10:28:02 2022

@author: steca

https://thepythoncorner.com/posts/2020-02-20-serialization-python-json/#:~:text=So%20how%20can%20we%20serialize,of%20the%20JSON%20serialization%20process.

- Il contatore lo incremento sempre dopo aver scritto i files del caso. In caso di ctrl+C
tra i files alla peggio, al giro dopo riprocesso qualcosa di già processato
"""

import numpy as np
import os
#from functions import *

import json
import re
import time
import sys
import glob


try:
    with open("./config.json", "r") as f:
        settings = json.load(f)
except:
    print("Sicuro che esista il file di config?")
    sys.exit(1)
    


#%% My functions

def compattaAscii(lstOfFiles, skiprows, compactPath):
    
    outFile = os.path.join(compactPath, "run"+re.split(r'\\|/', lstOfFiles[0])[-1][3:9]+".compact")
    print(f"Sto per scrivere {outFile}")
    
    with open(outFile, "w") as outf:
        
        for inFile in lstOfFiles:
            print(f"Reading {inFile}")
            with open(inFile, "r") as inf:
                
                for i,line in enumerate(inf):
                    if skiprows == 0:                        
                        outf.write(line)
                    elif i % skiprows == 0:
                        outf.write(line)

                        
    print(f"Ho scritto {outFile}")
    
    
    
def prepareNpz(compactFile, npzPath):
    
    outFile = os.path.join(npzPath, "run"+re.split(r'\\|/', compactFile)[-1][3:9]+".npz")
    print(f"Sto per scrivere {outFile}")

    
    myArr = np.loadtxt(compactFile)
    np.savez_compressed(outFile, data=myArr)
    print(f"Ho scritto {outFile}")




#%% Check folder exists

if not os.path.isdir(settings["asciiPath"]):
    print(f"Ascii path not valid\t{settings['asciiPath']}")
    sys.exit(1)
    
if not os.path.isdir(settings["compactPath"]):
    print(f"Compact path not valid\t{settings['compactPath']}")
    sys.exit(1)
    
if not os.path.isdir(settings["npzPath"]):
    print(f"Npz path not valid\t{settings['npzPath']}")
    sys.exit(1)
    
    

#%%

    

while(True):
    
    # Controllo chi sia l'ultima run
    
    # Supponendo che il file prima spill esista sempre,
    # creo il vettore dei numeri di run 
    # lstofAllRun = [int(re.split(r'\\|/', f)[-1][3:9]) for f in glob.glob("./ascii_seldom/*_000001.dat")]
    lstofAllRun = [int(re.split(r'\\|/', f)[-1][3:9]) for f in glob.glob(os.path.join(settings["asciiPath"],"*_000001.dat"))]
    lstofAllCompact = [int(re.split(r'\\|/', f)[-1][3:9]) for f in glob.glob(os.path.join(settings["compactPath"],"*.compact"))]
    lstofAllNpz = [int(re.split(r'\\|/', f)[-1][3:9]) for f in glob.glob(os.path.join(settings["npzPath"],"*.compact"))]
    
    # Ultima run eseguita. Potrebbe essere ancora in corso
    # Procedo solo se ho almeno una run nella vita
    if len(lstofAllRun) > 0:
        lastRun = np.sort(np.array(lstofAllRun)) [-1]
    else: 
        continue
        # wait till there exist at least one ascii file
    
    
    # # Aggiorno il dizionario
    # if lastRun != settings["lastAscii"]:
    #     settings["lastAscii"] = lastRun
    #     with open("config.json", "w") as f:
    #         json.dump(settings, f)
            
            
    # Controllo se ho ascii da compattare ma non della run in corso
    if settings["lastCompactedAscii"] < (lastRun-1):
        # Posso compattare un nuovo ascii
        

        # Se il numero successivo esiste tra le run, lo compatto
        if (settings['lastCompactedAscii'] + 1) in lstofAllRun:
            
            compattaAscii(glob.glob(os.path.join(settings["asciiPath"], f"run{settings['lastCompactedAscii']+1}*.dat")),
                          settings["skipRows"], settings["compactPath"] )
        
        
        
        # Incremento in ogni caso il contatore
        settings["lastCompactedAscii"] += 1
        
        # Lo salvo
        with open("config.json", "w") as f:
            json.dump(settings, f)
            
            
            
    # Procedo solo se ho almeno un compact nella vita
    if len(lstofAllCompact) > 0:
        lastCompact = np.sort(np.array(lstofAllCompact)) [-1]
    else: 
        continue
            
            
            
            
    # Controllo se ho dei compatti che posso trasformare in npz
    # Se è un compact sicuramente è pronto all'uso
    if settings["lastNumpyzedCompact"] < (lastCompact):
        
        if (settings['lastNumpyzedCompact'] + 1) in lstofAllCompact:
            prepareNpz(os.path.join(settings["compactPath"], f"run{settings['lastNumpyzedCompact']+1}.compact"),
                       settings["npzPath"])

        
        # Incremento in ogni caso il contatore
        settings["lastNumpyzedCompact"] += 1
        
        # Lo salvo
        with open("config.json", "w") as f:
            json.dump(settings, f)
            

        

        
        
    #print("Dormirò 10 secondi...")
    print(f"Value of variables\t lastCompactedAscii: {settings['lastCompactedAscii']}\t lastNumpyzedCompact: {settings['lastNumpyzedCompact']}" )
    time.sleep(1)

        
        
        
        