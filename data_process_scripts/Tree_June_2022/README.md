# Script june 2022
This script first compact all the spill's ASCII into a single ASCII, which is `run*.compact`. Then it writes everything into a ROOT TTree file, in this case without any structure, just the big matrix.
It has a couple of bugs, since it's not maintained anymore, take a look ad the [old repository](https://gitlab.com/stex6299/insulabtestbeam).