# HDF5
The script [HDF5.py](HDF5.py) looks for each new spill and append "in real time" in the correct HDF5 file. The beauty of this formact is that I can append in real time.

This script has been used in august 2022 both in H2 and T9




# Insulab test beam tools - Will be used in July-August 2022 [copied from old README]
The file [HDF5.py](HDF5.py) allows you to process and store ASCII files as they arrives, without waiting for the ending of the spill. Everyone can read these files in real time.

Inside the function `scriviDati`, you have to define the scheme of the data: in fact it's not store just a big matrix, but data are grouped. This way, we can avoid to load the waveforms. In future I plan to create a way to config outside the python.

In the [config.json](config.json) file you can set the directories for ASCII (usually mounted via sshfs), for the HDF5 files, for the amount of waveforms and to keep trace to the last processed file, even in case of crashes.

Before loading a file, there is a `try ... except` block, in order not to crash. If it can not access a file, it simple retry in the next iteration.

Based on a known bug from a previous version, now files are stored in cronological order (i.e. spill1, spill2, spill3...)


## Config.json [copied from old README]
The [configuration file](config.h5.json) is a simply json string where you can define:
- The path in which ascii files are `asciiPath`
- The path of where HDF5 will be put `HDF5`
- Number of digitized waveforms `numWaveform`
- Current run which is being processed `currAscii`
- Last processed spill of the `currAscii` run `currSpill`
- The path in which Root files will be saved `TreePath`. Actually not implemented


## Example of reading such a data
See an example [here](https://scarsi.web.cern.ch/MISC/Python/proveH5.html). Shortly:
```python3
import h5py

with h5py.File('data.h5', 'r', libver='latest', swmr=True) as hf:
	print(hf.keys())
	hf["sili"].shape
```

PLEASE, NOTE TO SPECIFY `swmr=True)` otherwise, if you don't close the file, it become impossible for me to append data

## Support tools
The file [hdfToRoot.py](/Python_useful_functions/hdfToRoot.py) allows you to translate every HDF5 file into a ROOT TTree file, by simply providing two directoriesù
```bash
$ python3 hdfToRoot.py /path/of/HDF5 /path/of/TTree
```








# Some info used during the TB


## H2 settembre 2022 - Klever

ssh insudaq@128.141.149.52


To have a look at the event number
```
bash
R=$(wc -l ASCII_SSHFS/run530150* | awk '{ print $1 }' | tail -1); echo $((R/9))
```


```
bash
history |grep -E "sshfs -o ro"

sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 ./ascii_daq_sshfs/
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/datadir_klever22 ./raw_daq_sshfs/
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 ./ASCII_SSHFS
```


{
    "asciiPath": "./ASCII_SSHFS",
    "HDF5": "/eos/project/i/insulab-como/testBeam/TB_H2_2022_09/HDF5",
    "numWaveform": 8,
    "currAscii": 530164,
    "currSpill": 420,
    "HDF5Marco": "/eos/project/i/insulab-como/testBeam/TB_H2_2022_09/HDF5_MARCO",
    "TreePath": "/eos/project/i/insulab-como/testBeam/TB_H2_2022_09/TREE"
}




## H2
ssh insudaq@128.141.149.52



sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_h2_storm ./ascii_daq_sshfs/  
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/datadir_h2_storm/ ./raw_daq_sshfs/


sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_h2_storm ./ASCII_SSHFS/




{
    "asciiPath": "./ASCII_SSHFS",
    "HDF5": "/eos/user/s/scarsi/testBeam/TB_H2_2022/HDF5",
    "numWaveform": 0,
    "currAscii": 520059,
    "currSpill": 15,
    "TreePath": "/eos/user/s/scarsi/testBeam/TB_H2_2022/TREE"
}






## T9

ssh insudaq@128.141.115.37
 
ascii_cern2022_t9_aug/  
datadir_cern2022_t9_aug/


sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.115.37:/data/insudaq/ascii_cern2022_t9_aug ./ascii_daq_sshfs  
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.115.37:/data/insudaq/datadir_cern2022_t9_aug ./raw_daq_sshfs

sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.115.37:/data/insudaq/ascii_cern2022_t9_aug ./ASCII_SSHFS



## Other useful stuffs

To list the processes who eats a lot of memory `ps aux | sort -nk +4 | grep autofs`

Kill by pid `sudo kill -9 230258 230257`
And then restart `service autofs start`

To dismount a sshfs folder (after the connection trips, to remount in the same place, you have to explicitely dismount `fusermount -u ./ascii_daq_sshfs`


## other notes
For marco i created the same files without swrm, for matlab compatibility