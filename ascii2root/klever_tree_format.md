### Klever 2022 test beam ROOT data format

- **xRaw[4]**: hit (cluster) position on the silicon strips detectors: 2 hi-res telescopes (0-2 cm) and 2 larger planes (0-10 cm) 
- **nStripHit[4]**: total number of strips hit for each silicon strip detector
- **nHit[4]**: total number of clusters for each silicon strip detector
- **digiBase[16]**: baselines of 2 x 8-channel waveform digitizers
- **digiPh[16]**: pulse height of the maximum for 2 x 8-channel waveform digitizers
- **digiTime[16]**: time of the maximum for 2 x 8-channel waveform digitizers
- **gonio[5]**: goniometer axes values
- **spill[1]**: spill number
- **step[1]**: step number
- **eventNumber[1]**: event number
- **wave0[1024]**: waveform of channel 0 of one of the waveform digitizer 
- **wave1[1024]**: waveform of channel 1 of one of the waveform digitizer 
- [...]
- **wave7[1024]**: waveform of channel 7 of one of the waveform digitizer 

Detailed output of `t->Print()`:
```
******************************************************************************
*Tree    :t         : Klever tree from ascii file 2022                       *
*Entries :    76621 : Total =      1271389172 bytes  File  Size =  452728381 *
*        :          : Tree compression factor =   2.81                       *
******************************************************************************
*Br    0 :xRaw      : xRaw[4]/F                                              *
*Entries :    76621 : Total  Size=    1235131 bytes  File Size  =     862637 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=   1.43     *
*............................................................................*
*Br    1 :nStripHit : nStripHit[4]/s                                         *
*Entries :    76621 : Total  Size=     622684 bytes  File Size  =     120973 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=   5.13     *
*............................................................................*
*Br    2 :nHit      : nHit[4]/s                                              *
*Entries :    76621 : Total  Size=     622159 bytes  File Size  =      12912 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=  48.00     *
*............................................................................*
*Br    3 :digiBase  : digiBase[16]/s                                         *
*Entries :    76621 : Total  Size=    2461485 bytes  File Size  =     827073 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=   2.97     *
*............................................................................*
*Br    4 :digiPh    : digiPh[16]/s                                           *
*Entries :    76621 : Total  Size=    2461275 bytes  File Size  =    1401413 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=   1.75     *
*............................................................................*
*Br    5 :digiTime  : digiTime[16]/s                                         *
*Entries :    76621 : Total  Size=    2461485 bytes  File Size  =    1286819 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=   1.91     *
*............................................................................*
*Br    6 :gonio     : gonio[5]/F                                             *
*Entries :    76621 : Total  Size=    1541720 bytes  File Size  =      25089 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=  61.36     *
*............................................................................*
*Br    7 :spill     : spill/I                                                *
*Entries :    76621 : Total  Size=     315778 bytes  File Size  =      12192 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=  25.71     *
*............................................................................*
*Br    8 :step      : step/I                                                 *
*Entries :    76621 : Total  Size=     315673 bytes  File Size  =      12034 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=  26.04     *
*............................................................................*
*Br    9 :eventNumber : eventNumber/I                                        *
*Entries :    76621 : Total  Size=     316408 bytes  File Size  =     140741 *
*Baskets :      101 : Basket Size=      32000 bytes  Compression=   2.23     *
*............................................................................*
*Br   10 :wave0     : wave0[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   55574145 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.83     *
*............................................................................*
*Br   11 :wave1     : wave1[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   54981705 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.86     *
*............................................................................*
*Br   12 :wave2     : wave2[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   56353537 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.79     *
*............................................................................*
*Br   13 :wave3     : wave3[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   55830004 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.82     *
*............................................................................*
*Br   14 :wave4     : wave4[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   56365567 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.79     *
*............................................................................*
*Br   15 :wave5     : wave5[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   56756669 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.77     *
*............................................................................*
*Br   16 :wave6     : wave6[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   55748186 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.82     *
*............................................................................*
*Br   17 :wave7     : wave7[1024]/s                                          *
*Entries :    76621 : Total  Size=  157379361 bytes  File Size  =   56052270 *
*Baskets :     5160 : Basket Size=      32000 bytes  Compression=   2.81     *
*............................................................................*
```
