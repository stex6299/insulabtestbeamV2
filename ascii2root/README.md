#Stefano notes
Da qui sotto in poi sono cose clonate da Valerio. Ho copiato solo la cartella ascii2root. Appunti 


Per avere il comando ROOT. Magari mettere nel bashrc
```
bash
source /home/scarsi-home/root/bin/thisroot.sh
```


Montare la cartella
Klever 2022-09
```
bash
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 ./ASCII_SSHFS
```

Eseguire
```
bash
auto_update_root_files.sh
```

Comodo per un for
```
bash
for r in {1..5}; do echo ./update_single_run.sh $r ; done
```







# From ASCII to ROOT

Scripts to create ROOT files - Klever test beam, SPS, Aug 2021

### Installation

If not already done, clone the repo:
```bash
git clone https://baltig.infn.it/insulab/tbtools.git 
```
### Running the script

```bash
cd ascii2root
./update_single_run.sh
```
This script detects the current run from the `LOCAL_ASCII` dir and launch the `update_single_run.sh` script.

### Output data format

For the used data type of the tree branches, refer to [machine independent data types](https://root.cern/manual/root_classes_data_types_and_global_variables/#machine-independent-data-types).

For the Klever data refer to [Klever 2021 testbeam data format](https://baltig.infn.it/insulab/tbtools/-/blob/master/ascii2root/klever_tree_format.md)

### Note
```bash
git remote set-url origin git@github.com:maskass/root4klever.git
```
