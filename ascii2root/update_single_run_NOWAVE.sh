#!/bin/bash
if [ $# -ne 1 ];then
    echo "ERROR: use 1 argument (run number)"
    exit 1
fi

nrun=$( printf "%6.6d" "$1")

ascii_dir="$HOME/LOCAL_ASCII"
root_dir="$HOME/LOCAL_ROOT"
root_merged_dir="/eos/user/m/mmoulson/KLEVER/TestBeamData/Root2021/merged"

# for every spill of a given run, check the root dir and
# create the root file if it does not exist

root_merged_file=$(awk -v run=$nrun 'BEGIN{printf("run%6.6d.root",run)}')

new_file_count=0;

for file in $(ls $ascii_dir"/run"$nrun"_nowave_"*.dat); do
    nspill=$(echo $file | awk '{c=split($1,a,"/");split(a[c],b,".");dd=split(b[1],d,"_");print d[dd]+0}')

    root_file=$(awk -v run=$nrun -v spill=$nspill 'BEGIN{printf("run%6.6d_%6.6d.root",run,spill)}')

    
    # check file existence
    if [ ! -e $root_dir/$root_file ];then
	new_file_count=$(echo $new_file_count + 1 | bc)
	root -l -b -q 'ascii2rootNOWAVE.cc+('\"$file\"','\"$root_dir/$root_file\"')'

	## Appending each new file to the merged one
	hadd -a $root_merged_dir/$root_merged_file $root_dir/$root_file
    fi
    
done
#echo $new_file_count


if [ $new_file_count -gt 0 ];then
    echo $new_file_count" files appended to "$root_merged_dir/$root_merged_file 
#    echo "Appendedcreating new merged file..."
#    hadd -f $root_merged_dir/$root_merged_file $root_dir/run$nrun*.root
#    echo " "
#    echo "... done."

    #### This part has nothing to do with the ROOT files,
    #### I'm just using the script to create the npz file
    #### but ONLY IF the root file size is < 100 MB!
    numpy_dir="/eos/user/v/vmascagn/STORM_2021/testbeam_data/NUMPY"
    
    byte_file_size=$(echo $(stat -c "%s" $root_merged_dir/$root_merged_file) / 1024 / 1024 | bc)
    
    if [ $byte_file_size -lt 100 ] ; then 
#    if [ $(echo $(date +%s) " % 2 "| bc) -eq 0 ]; then
        echo -n "Running root2numpy... (filesize is "$byte_file_size" MB)"
        python3 ../ascii2numpy/root2numpy.py $root_merged_dir/$root_merged_file $numpy_dir/run$nrun.npz
        echo " done."
	echo "Created "$numpy_dir/run$nrun.npz
    fi
fi
