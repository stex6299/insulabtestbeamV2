# Insulab test beam tools
This is the second version, neater than [the previous](https://gitlab.com/stex6299/insulabtestbeam).

- [rsync](./rsync) contains two scripts for syncronizing the raw and ascii files produced by the DAQ PC on the insulab cernbox group (EOS path: `/eos/project/i/insulab-como`). They are meant to be executing in a tmux session
- [gechino](./gechino) contains some scripts for controlling the *CAEN DT5485P*, usually referred as *gechino*. These scripts should be put on the "PC" connected with him. They have to be called as python scripts
- [Python_useful_functions](./Python_useful_functions) contains some python codes, both as scripts meant to be directly executed or example of functions to be embedded in a script
- [data_process_scripts](./data_process_scripts) contains the scripts used during testbeams for processing the data. The main script for each folder is meant to be executing in a tmux session
- [ascii2root](./ascii2root) contains ROOT scripts derived from Valerio's one for creating root files
