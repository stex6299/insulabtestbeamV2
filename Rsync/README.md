# Rsync scripts
The purpose of this scripts is to syncronize on insulab cernbox `/eos/project/i/insulab-como` raw and ascii data directly from the DAQ PC. Everyone who needs to perform an online/offline analysis should mount these EOS folders via sshfs, in order not to directly connect to the DAQ PC.

To be allowed to see these files, you should be in the CERN e-group `cernbox-project-insulab-como-readers`: [ask Stefano](mailto:scarsi@studenti.uninsubria.it) for more info.

Keep in mind the importance of the leading slash in the source directory. [Cfr for example](https://www.digitalocean.com/community/tutorials/how-to-use-rsync-to-sync-local-and-remote-directories)

# List of features
- `sudo chmod +x *.sh`
- It is a good idea to run the scripts as `k5reauth ./script.sh`. It helps me to always have a valid token

*This trailing slash signifies the contents of dir1. Without the trailing slash, dir1, including the directory, would be placed within dir2.*


# How to mount
In the folders `ascii_daq_sshfs` and `raw_daq_sshfs` you should mount the folder on the DAQ PC

# To transfer huge amounts of data
This command is way better
```bash
xrdcp ./tmpSSHFS/* root://eosuser//eos/project/i/insulab-como/testBeam/TB_H2_2022_09/reprocessed_ASCII_MICHELA/datadir
```

## Example of usage:

H2 2022-09
```bash
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 ./ascii_daq_sshfs/
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/datadir_klever22 ./raw_daq_sshfs/
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_klever22 ./ASCII_SSHFS
```


H2 2022-08
```bash
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/ascii_h2_storm ./ascii_daq_sshfs/
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.149.52:/data/insudaq/datadir_h2_storm/ ./raw_daq_sshfs/
```


T9 2022-08
```bash
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.115.37:/data/insudaq/ascii_cern2022_t9_aug ./ascii_daq_sshfs
sshfs -o ro -o reconnect,ServerAliveInterval=15 insudaq@128.141.115.37:/data/insudaq/datadir_cern2022_t9_aug ./raw_daq_sshfs
```